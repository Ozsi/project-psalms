package org.ozsi.web;

import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Laszlo_Ozsvart
 */
@Controller
@RequestMapping("/session")
public class SessionController {

    @RequestMapping(method= RequestMethod.GET)
    public @ResponseBody
    String getSession(HttpSession httpSession, @RequestParam(value="name", required=false) String name) {
        String result;
        if (name != null) {
            httpSession.setAttribute("name", name);
            result = "Hello, " + name + "!";
        } else {
            result = Optional.ofNullable(httpSession.getAttribute("name"))
                .map(storedName -> "Welcome back, " + storedName + "!")
                .orElse("Please provide a name :)");
        }
        return result;
    }

}
