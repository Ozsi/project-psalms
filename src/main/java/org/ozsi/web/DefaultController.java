package org.ozsi.web;

import org.ozsi.domain.RequestParams;
import org.ozsi.service.WelcomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DefaultController {

    private WelcomeService welcomeService;

    @Autowired
    public DefaultController(WelcomeService welcomeService) {
        this.welcomeService = welcomeService;
    }

    @RequestMapping("/")
    String home(@ModelAttribute RequestParams params) {
        return welcomeService.doWelcome();
    }

}
