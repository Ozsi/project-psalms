package org.ozsi.web;

import java.util.concurrent.atomic.AtomicLong;

import org.ozsi.domain.Access;
import org.ozsi.domain.Greeting;
import org.ozsi.repository.AccessRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/hello-world")
public class HelloWorldController {
    private static final String TEMPLATE = "Hello, %s!";

    private final AtomicLong counter = new AtomicLong();

    private AccessRepository accessRepository;

    @Autowired
    public HelloWorldController(AccessRepository accessRepository) {
        this.accessRepository = accessRepository;
    }

    @RequestMapping(method=RequestMethod.GET)
    public @ResponseBody
    Greeting sayHello(@RequestParam(value="name", required=false, defaultValue="Stranger") String name) {
        accessRepository.save(createAccess(name));
        return new Greeting(counter.incrementAndGet(), String.format(TEMPLATE, name));
    }

    private Access createAccess(String name) {
        Access access = new Access();
        access.setId(counter.intValue());
        access.setName(name);
        return access;
    }

}
