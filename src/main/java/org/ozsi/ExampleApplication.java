package org.ozsi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class ExampleApplication {

    public static void main(String[] args) throws Exception {
        SpringApplication app = new SpringApplication(ExampleApplication.class);
        app.run(args);
    }

}
