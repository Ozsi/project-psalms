package org.ozsi.service;

import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.ozsi.domain.Access;
import org.ozsi.repository.AccessRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WelcomeService {

    private AccessRepository accessRepository;

    @Autowired
    public WelcomeService(AccessRepository accessRepository) {
        this.accessRepository = accessRepository;
    }

    public String doWelcome() {
        return StreamSupport.stream(accessRepository.findAll().spliterator(), false)
            .map(Access::toString)
            .collect(Collectors.joining(",<br/>"));
    }
}
