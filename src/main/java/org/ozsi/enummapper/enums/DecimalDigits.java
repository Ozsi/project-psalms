package org.ozsi.enummapper.enums;

public enum DecimalDigits {
    ZERO, ONE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE
}
