package org.ozsi.domain;

import java.util.Objects;

import org.springframework.data.cassandra.mapping.PrimaryKey;
import org.springframework.data.cassandra.mapping.Table;

/**
 * @author Laszlo_Ozsvart
 */
@Table("access")
public class Access {
    @PrimaryKey
    private int id;
    private String name;

    public Access() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Access{" +
            "id=" + id +
            ", name='" + name + '\'' +
            '}';
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final Access other = (Access) obj;
        return Objects.equals(this.id, other.id)
            && Objects.equals(this.name, other.name);
    }
}
