package org.ozsi.enummapper.mapper;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class EnumMapper<FROM extends Enum<FROM>, TO extends Enum<TO>> {

    private final Map<FROM, TO> map;

    public EnumMapper(Class<FROM> fromClass, Class<TO> toClass) {
        try {
            map = Arrays.stream(fromClass.getEnumConstants())
                    .collect(Collectors.toMap(Function.identity(), value -> TO.valueOf(toClass, value.name())));
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("Cannot create enum mapping from " + fromClass.getName() + " to " + toClass.getName(), e);
        }
    }

    public TO get(FROM value) {
        return map.get(value);
    }

    public Map<FROM, TO> getMap() {
        return map;
    }
}
