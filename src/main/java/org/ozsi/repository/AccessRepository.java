package org.ozsi.repository;

import org.ozsi.domain.Access;
import org.springframework.data.repository.CrudRepository;

/**
 * @author Laszlo_Ozsvart
 */

public interface AccessRepository extends CrudRepository<Access, Integer> {

}
