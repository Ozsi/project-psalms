package org.ozsi.enummapper;

import org.ozsi.enummapper.enums.BinaryDigits;
import org.ozsi.enummapper.enums.DecimalDigits;
import org.ozsi.enummapper.mapper.EnumMapper;

public class App {

    public static EnumMapper<BinaryDigits, DecimalDigits> getBinaryToDecimalMapper() {
        return new EnumMapper<>(BinaryDigits.class, DecimalDigits.class);
    }

    public static EnumMapper<DecimalDigits, BinaryDigits> getDecimalToBinaryMapper() {
        return new EnumMapper<>(DecimalDigits.class, BinaryDigits.class);
    }

    public static void main(String[] args) {
        EnumMapper<BinaryDigits, DecimalDigits> mapper = getBinaryToDecimalMapper();
        getDecimalToBinaryMapper();
    }
}
