package org.ozsi.enummapper.enums;

public enum BinaryDigits {
    ZERO, ONE
}
